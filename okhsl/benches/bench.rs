#![feature(test)]

extern crate test;

use okhsl::*;
use test::Bencher;

#[bench]
fn to_nop(b: &mut Bencher) {
    b.iter(|| {
        for c in 0..255*255 {
            let r = (c / 7) as u8;
            let g = (c >> 8) as u8 ^ 0xE7;
            let b = c as u8 ^ 0x55;
            test::black_box(Rgb { r, g, b });
        }
    })
}

#[bench]
fn to_ok(b: &mut Bencher) {
    b.iter(|| {
        for c in 0..255*255 {
            let r = (c / 7) as u8;
            let g = (c >> 8) as u8 ^ 0xE7;
            let b = c as u8 ^ 0x55;
            test::black_box(Oklab::from(Rgb { r, g, b }));
        }
    })
}

#[bench]
fn to_okhsv(b: &mut Bencher) {
    b.iter(|| {
        for c in 0..255*255 {
            let r = (c / 7) as u8;
            let g = (c >> 8) as u8 ^ 0xE7;
            let b = c as u8 ^ 0x55;
            test::black_box(Okhsv::from(Rgb { r, g, b }));
        }
    })
}

#[bench]
fn to_okhsl(b: &mut Bencher) {
    b.iter(|| {
        for c in 0..255*255 {
            let r = (c / 7) as u8;
            let g = (c >> 8) as u8 ^ 0xE7;
            let b = c as u8 ^ 0x55;
            test::black_box(Okhsl::from(Rgb { r, g, b }));
        }
    })
}

#[bench]
fn from_ok(b: &mut Bencher) {
    b.iter(|| {
        for c in 0..255*255 {
            let a = ((c / 7) as u8) as f32 / 255.;
            let b = ((c >> 8) as u8 ^ 0xE7) as f32 / 255.;
            let l = (c as u8 ^ 0x55) as f32 / 255.;
            test::black_box(Oklab { l, a, b }.to_srgb());
        }
    })
}

#[bench]
fn from_okhsv(b: &mut Bencher) {
    b.iter(|| {
        for c in 0..255*255 {
            let h = ((c / 7) as u8) as f64 / 255.;
            let s = ((c >> 8) as u8 ^ 0xE7) as f32 / 255.;
            let v = (c as u8 ^ 0x55) as f32 / 255.;
            test::black_box(Okhsv { h, s, v }.to_srgb());
        }
    })
}

#[bench]
fn from_okhsl(b: &mut Bencher) {
    b.iter(|| {
        for c in 0..255*255 {
            let h = ((c / 7) as u8) as f64 / 255.;
            let s = ((c >> 8) as u8 ^ 0xE7) as f32 / 255.;
            let l = (c as u8 ^ 0x55) as f32 / 255.;
            test::black_box(Okhsl { h, s, l }.to_srgb());
        }
    })
}
