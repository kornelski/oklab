use okhsl::*;

#[test]
fn hsl_roundtrips1() {
    hsl_roundtrips_inner(0..128);
}

#[test]
fn hsl_roundtrips2() {
    hsl_roundtrips_inner(128..256);
}

#[cfg(test)]
fn hsl_roundtrips_inner(red_range: std::ops::Range<u16>) {
    use rgb::Rgb;

    let mut total_diff = 0.;
    let mut max_diff = 0.;
    for r in red_range {
        for g in 0..256u16 {
            for b in 0..256u16 {
                let p1 = Rgb::new(
                    f32::from(r) / 255.0,
                    f32::from(g) / 255.0,
                    f32::from(b) / 255.0,
                );
                let lab = srgb_f32_to_oklab(p1);
                let hsl = oklab_to_okhsl(lab);
                assert!(hsl.h.is_finite());
                assert!(hsl.l.is_finite());
                assert!(hsl.s.is_finite(), "{hsl:?} {lab:?} {p1}");
                let lab_out = okhsl_to_oklab(hsl);
                let p2 = oklab_to_srgb_f32(lab_out);
                assert_eq!(lab.to_srgb(), lab_out.to_srgb());
                let diff = (p1.b - p2.b).mul_add(
                    p1.b - p2.b,
                    (p1.g - p2.g).mul_add(p1.g - p2.g, (p1.r - p2.r).powi(2)),
                );
                if diff > max_diff {
                    max_diff = diff;
                    eprintln!("HSL: {p1:.4?} -> {p2:.4?} diff {max_diff:0.5} {hsl:?} lab={lab:0.2?}");
                }
                total_diff += f64::from(diff);
            }
        }
    }
    assert!(max_diff < 0.000006, "{max_diff}");
    assert!(total_diff < 0.000025, "{total_diff}");
}

#[test]
fn hsv_roundtrips1() {
    hsv_roundtrips_inner(0..128);
}

#[test]
fn hsv_roundtrips2() {
    hsv_roundtrips_inner(128..256);
}

#[cfg(test)]
fn hsv_roundtrips_inner(red_range: std::ops::Range<u16>) {
    use rgb::Rgb;

    let mut total_diff = 0.;
    let mut max_diff = 0.;
    for r in red_range {
        for g in 0..256u16 {
            for b in 0..256u16 {
                let p1 = Rgb::new(
                    f32::from(r) / 255.0,
                    f32::from(g) / 255.0,
                    f32::from(b) / 255.0,
                );
                let lab = srgb_f32_to_oklab(p1);
                let hsv = oklab_to_okhsv(lab);
                assert!(hsv.h.is_finite());
                assert!(hsv.s.is_finite(), "bad hsv: {hsv:?} {lab:?} {p1:?}");
                assert!(hsv.v.is_finite());
                let lab_out = okhsv_to_oklab(hsv);
                let p2 = oklab_to_srgb_f32(lab_out);
                assert_eq!(lab.to_srgb(), lab_out.to_srgb());
                let diff = (p1.b - p2.b).mul_add(
                    p1.b - p2.b,
                    (p1.g - p2.g).mul_add(p1.g - p2.g, (p1.r - p2.r).powi(2)),
                );
                if diff > max_diff {
                    max_diff = diff;
                    eprintln!("HSV: {p1:.4?} -> {p2:.4?} diff {max_diff:0.5} {hsv:?} lab={lab:0.2?}");
                }
                total_diff += f64::from(diff);
            }
        }
    }
    assert!(max_diff < 0.00000025, "{max_diff}");
    assert!(total_diff < 0.000025, "{total_diff}");
}
