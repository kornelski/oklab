# Convert sRGB to Okhsl/Okhsv (Oklab-based) perceptual color space

A simple Rust implementation of Okhsl and Okhsv color conversion routines based on the reference implementation in the [blog post](https://bottosson.github.io/posts/colorpicker/).

Okhsl and Okhsv color spaces are meant to have more orhogonal hue, saturation, and lightness than the basic HSL/HSV colors.

## API

```rust
use okhsl::*;

let lab = Oklab::from(Rgb {r: 1, g: 127, b: 255});
let Oklab {l, a, b} = lab;

let hsv = Okhsv::from(lab);
let Okhsv {h, s, v} = hsv;

let lab = hsv.to_oklab();
let rgb = hsv.to_srgb();
```

Oklab components are floats. `l` is in range 0 to 1, and `a`/`b` are small numbers that *can be negative*.

Okhsv/Okhsl components are floats. HSL values are approximately in range 0 to 1 (inclusive). You can expect to roundtrip `Okhsl`⇔`Oklab` and `Okhsv`⇔`Oklab` with little loss of precision.

## Examples

[Usage examples](https://gitlab.com/kornelski/oklab/-/tree/main/okhsl/examples)

```bash
cargo run -r --example invert -- some_image.jpeg
```
