use okhsl::Okhsl;
use rgb::prelude::*;
use std::path::PathBuf;

fn main() {
    let input_path = PathBuf::from(std::env::args_os().nth(1).expect("Please provide an image file"));
    let mut img = load_image::load_path(&input_path).unwrap().into_rgba().0;

    for px in img.pixels_mut() {
        *px = px.map_colors(|c| 255 - c);
    }

    let (buf, w, h) = img.as_contiguous_buf();
    lodepng::encode32_file(input_path.with_extension("basic-inverted.png"), buf, w, h).unwrap();

    for px in img.pixels_mut() {
        *px = px.map_colors(|c| 255 - c); // undo

        let mut hsl = Okhsl::from(px.rgb());
        hsl.l = 1. - hsl.l;
        *px = hsl.to_srgb().with_alpha(px.a);
    }

    let (buf, w, h) = img.as_contiguous_buf();
    lodepng::encode32_file(input_path.with_extension("ok-inverted.png"), buf, w, h).unwrap();
}
