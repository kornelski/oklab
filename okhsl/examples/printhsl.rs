use okhsl::*;

fn main() {
    for r in (0..=255).step_by(17) {
        for g in (0..=255).step_by(17) {
            for b in (0..=255).step_by(17) {
                let hsl = oklab_to_okhsl(srgb_to_oklab(Rgb { r, g, b }));
                println!("#{r:02x}{g:02x}{b:02x} = {hsl:0.3?}");
            }
        }
    }

    let mut min_h = f64::INFINITY;
    let mut min_s = f32::INFINITY;
    let mut min_v = f32::INFINITY;
    let mut max_h = -f64::INFINITY;
    let mut max_s = -f32::INFINITY;
    let mut max_v = -f32::INFINITY;
    for r in 0..=255 {
        for g in 0..=255 {
            for b in 0..=255 {
                let lab = srgb_to_oklab(Rgb { r, g, b });
                let hsv = oklab_to_okhsv(lab);
                max_h = max_h.max(hsv.h);
                max_s = max_s.max(hsv.s);
                max_v = max_v.max(hsv.v);
                min_h = min_h.min(hsv.h);
                min_s = min_s.min(hsv.s);
                min_v = min_v.min(hsv.v);
            }
        }
    }
    println!("h = {min_h}...{max_h}");
    println!("s = {min_s}...{max_s}");
    println!("v = {min_v}...{max_v}");

    let mut min_h = f64::INFINITY;
    let mut min_s = f32::INFINITY;
    let mut min_l = f32::INFINITY;
    let mut max_h = -f64::INFINITY;
    let mut max_s = -f32::INFINITY;
    let mut max_l = -f32::INFINITY;
    for r in 0..=255 {
        for g in 0..=255 {
            for b in 0..=255 {
                let lab = srgb_to_oklab(Rgb { r, g, b });
                let hsl = oklab_to_okhsl(lab);
                max_h = max_h.max(hsl.h);
                max_s = max_s.max(hsl.s);
                max_l = max_l.max(hsl.l);
                min_h = min_h.min(hsl.h);
                min_s = min_s.min(hsl.s);
                min_l = min_l.min(hsl.l);
            }
        }
    }
    println!("h = {min_h}...{max_h}");
    println!("s = {min_s}...{max_s}");
    println!("l = {min_l}...{max_l}");
}
