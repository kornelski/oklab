# Convert sRGB to Oklab perceptual color space

Simple Rust implementation of the Oklab color space conversion routines based on the reference implementation in the [blog post](https://bottosson.github.io/posts/oklab/).

Mixing of colors in the Oklab color space is meant to give more pleasing gradients.

## API

```rust
use oklab::*;
let Oklab {l, a, b} = srgb_to_oklab(RGB {r, g, b});
let rgb = oklab_to_srgb(Oklab {l, a, b});
```

Oklab components are floats. `l` is ligthness in range 0 to 1 (inclusive), and `a`/`b` colors are small numbers that *can be negative*.


