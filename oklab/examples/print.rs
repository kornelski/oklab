use oklab::{srgb_to_oklab, Rgb};

fn main() {
    for r in (0..=255).step_by(17) {
        for g in (0..=255).step_by(17) {
            for b in (0..=255).step_by(17) {
                let lab = srgb_to_oklab(Rgb { r, g, b });
                println!("#{r:02x}{g:02x}{b:02x} = {lab:0.3?}");
            }
        }
    }

    let mut min_a = f32::INFINITY;
    let mut min_b = f32::INFINITY;
    let mut max_a = -f32::INFINITY;
    let mut max_b = -f32::INFINITY;
    for r in 0..=255 {
        for g in 0..=255 {
            for b in 0..=255 {
                let lab = srgb_to_oklab(Rgb { r, g, b });
                max_a = max_a.max(lab.a);
                max_b = max_b.max(lab.b);
                min_a = min_a.min(lab.a);
                min_b = min_b.min(lab.b);
            }
        }
    }
    println!("a = {min_a}...{max_a}\nb = {min_b}...{max_b}");
}
